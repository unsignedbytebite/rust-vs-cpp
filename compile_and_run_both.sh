#!/bin/bash

# Clean
echo ""
echo "[ Cleaning]"
echo ""

rm ./cpp/bin -r
mkdir ./cpp/bin
mkdir ./cpp/bin/release
mkdir ./cpp/bin/debug
rm ./rust/prime_calculator/target -r

echo "Cleaned"

# Build
echo ""
echo "[ Build Rust Debug]"
echo ""
cd ./rust/prime_calculator/
time cargo build
cd ../../

echo ""
echo "[ Build Clang Debug]"
echo ""
cd ./cpp/
time clang++ -std=c++17 -g -o./bin/debug/prime_calculator ./prime_calculator.cpp
cd ../

echo ""
echo "[ Build Rust Release]"
echo ""
cd ./rust/prime_calculator/
time cargo build --release
#cargo test --release
cd ../../

echo ""
echo "[ Build Clang Release]"
echo ""
cd ./cpp/
time clang++ -O2 -std=c++17 -o ./bin/release/prime_calculator ./prime_calculator.cpp
cd ../

# Run
echo ""
echo "[ Run Rust Debug]"
echo ""
wc -c ./rust/prime_calculator/target/debug/prime_calculator
./rust/prime_calculator/target/debug/prime_calculator

echo ""
echo "[ Run Clang Debug]"
echo ""
wc -c ./cpp/bin/debug/prime_calculator
./cpp/bin/debug/prime_calculator

echo ""
echo "[ Run Rust Release]"
echo ""
wc -c ./rust/prime_calculator/target/release/prime_calculator
./rust/prime_calculator/target/release/prime_calculator

echo ""
echo "[ Run Clang Release]"
echo ""
wc -c ./cpp/bin/debug/prime_calculator
./cpp/bin/debug/prime_calculator

